import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';

class RoundedButton extends StatefulWidget {
  final String label;
  final Color backgroundColor;
  final Color textColor;
  final double verticalPadding;
  final double horizontalPadding;
  final bool enabled;
  final bool elevated;
  final double radius;
  final Function()? onPressed;

  const RoundedButton({
    Key? key,
    required this.label,
    this.backgroundColor = orangeColor,
    this.textColor = Colors.white,
    this.verticalPadding = 14,
    this.horizontalPadding = 32,
    this.enabled = true,
    this.elevated = true,
    this.radius = 120,
    this.onPressed,
  }) : super(key: key);

  @override
  _RoundedButtonState createState() => _RoundedButtonState();
}

class _RoundedButtonState extends State<RoundedButton> {
  bool _isPressed = false;

  @override
  Widget build(BuildContext context) {
    if (!widget.enabled) {
      return Text(
        widget.label,
        textAlign: TextAlign.center,
        style: TextStyle(color: mediumGreyColor, fontWeight: FontWeight.w700),
      )
          .padding(
            vertical: widget.verticalPadding,
            horizontal: widget.horizontalPadding,
          )
          .borderRadius(all: widget.radius)
          .backgroundColor(lightGreyColor)
          .clipRRect(all: widget.radius);
    }

    return Text(
      widget.label,
      textAlign: TextAlign.center,
      style: TextStyle(color: widget.textColor, fontWeight: FontWeight.w700),
    )
        .padding(
          vertical: widget.verticalPadding,
          horizontal: widget.horizontalPadding,
        )
        .borderRadius(all: widget.radius)
        .ripple()
        .backgroundColor(widget.backgroundColor, animate: true)
        .clipRRect(all: widget.radius)
        .borderRadius(all: widget.radius, animate: true)
        .boxShadow(
          color: widget.backgroundColor.withOpacity(.45),
          offset: Offset(0, 18),
          spreadRadius: _isPressed ? -17 : -15,
          blurRadius: 8,
        )
        .gestures(
          onTapChange: (tapStatus) => setState(() => _isPressed = tapStatus),
          onTap: widget.onPressed,
        )
        .scale(all: _isPressed ? 0.95 : 1.0, animate: true)
        .animate(Duration(milliseconds: 175), Curves.fastLinearToSlowEaseIn);
  }
}
