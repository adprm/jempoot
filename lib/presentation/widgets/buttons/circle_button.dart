import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';

class CircleButton extends StatefulWidget {
  final Widget child;
  final Color backgroundColor;
  final Function()? onPressed;
  final double padding;

  const CircleButton({
    Key? key,
    required this.child,
    this.backgroundColor = orangeColor,
    this.onPressed,
    this.padding = 12,
  }) : super(key: key);

  @override
  _CircleButtonState createState() => _CircleButtonState();
}

class _CircleButtonState extends State<CircleButton> {
  bool _isPressed = false;

  @override
  Widget build(BuildContext context) {
    return Styled.widget(child: widget.child)
        .padding(vertical: widget.padding, horizontal: widget.padding)
        .borderRadius(all: 150)
        .ripple()
        .backgroundColor(widget.backgroundColor, animate: true)
        .clipRRect(all: 150)
        .borderRadius(all: 150, animate: true)
        .elevation(
          _isPressed ? 2 : 4,
          borderRadius: BorderRadius.circular(150),
          shadowColor: widget.backgroundColor == Colors.white
              ? Colors.black.withOpacity(.38)
              : widget.backgroundColor.withOpacity(.38),
        )
        .boxShadow(
          color: widget.backgroundColor == Colors.white
              ? Colors.black.withOpacity(.06)
              : widget.backgroundColor.withOpacity(.06),
          offset: Offset(0, 10),
          spreadRadius: _isPressed ? -10 : -8,
          blurRadius: 8,
        )
        .gestures(
          onTapChange: (tapStatus) => setState(() => _isPressed = tapStatus),
          onTap: widget.onPressed,
        )
        .scale(all: _isPressed ? 0.95 : 1.0, animate: true)
        .animate(Duration(milliseconds: 175), Curves.fastLinearToSlowEaseIn);
  }
}
