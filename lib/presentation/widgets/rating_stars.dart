import 'package:flutter/material.dart';

import '../../core/themes/theme.dart';

class RatingStars extends StatelessWidget {
  final double voteAverage;
  final double starSize;
  final double fontSize;

  RatingStars({
    Key? key,
    this.voteAverage = 0,
    this.starSize = 20.0,
    this.fontSize = 12.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int n = voteAverage.round();

    List<Widget> widgets = List.generate(5, (index) {
      return Icon(
        Icons.star,
        color: index < n ? Colors.amber : darkGreyColor,
        size: starSize,
      );
    });

    widgets.add(SizedBox(width: 4.0));
    widgets.add(Text(
      '$voteAverage',
      style: TextStyle(fontSize: fontSize),
    ));

    return Row(
      children: widgets,
    );
  }
}
