import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';
import '../../widgets/food_card.dart';
import '../../widgets/buttons/circle_button_alt.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: SvgPicture.asset('assets/logo.svg', height: 32),
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/favorite');
            },
            icon: Icon(Icons.favorite),
            color: orangeColor,
          ),
          IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/notification');
            },
            icon: Icon(Icons.notifications),
            color: orangeColor,
          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        child: Column(
          children: [
            <Widget>[
              Container(
                height: 210,
                color: orangeColor,
              ),
              Column(
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
                    height: 125,
                    width: mediaQuery.size.width,
                    decoration: BoxDecoration(
                      color: mediumGreyColor,
                      borderRadius: BorderRadius.circular(defaultBorderRadius),
                      image: DecorationImage(
                        image: AssetImage('assets/banner_1.png'),
                        fit: BoxFit.cover,
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          offset: Offset(0, 20),
                          spreadRadius: -15,
                          blurRadius: 12,
                        ),
                      ],
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircleButtonAlt(
                        onPressed: () {
                          Navigator.pushNamed(context, '/food');
                        },
                        icon: SvgPicture.asset('assets/ic_food.svg'),
                        label: 'Food',
                      ).padding(horizontal: 8),
                      CircleButtonAlt(
                        icon: SvgPicture.asset('assets/ic_jempoot.svg'),
                        label: 'Jempoot',
                      ).padding(horizontal: 8),
                      CircleButtonAlt(
                        icon: SvgPicture.asset('assets/ic_kirim.svg'),
                        label: 'Kirim',
                      ).padding(horizontal: 8),
                      CircleButtonAlt(
                        icon: SvgPicture.asset('assets/ic_pulsa.svg'),
                        label: 'Pulsa / Tagihan',
                        disabled: true,
                      ).padding(horizontal: 8),
                    ],
                  ).padding(horizontal: 16),
                ],
              ),
            ].toStack(),
            SizedBox(height: 24),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Promo Hari Ini',
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                ),
                InkResponse(
                  onTap: () {},
                  child: Text(
                    'Selengkapnya',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: greenColor,
                    ),
                  ),
                ),
              ],
            ).padding(horizontal: 16),
            Container(
              margin: const EdgeInsets.all(16),
              height: 125,
              width: mediaQuery.size.width,
              decoration: BoxDecoration(
                color: mediumGreyColor,
                borderRadius: BorderRadius.circular(defaultBorderRadius),
                image: DecorationImage(
                  image: AssetImage('assets/banner_2.png'),
                  fit: BoxFit.cover,
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0, 20),
                    spreadRadius: -15,
                    blurRadius: 12,
                  ),
                ],
              ),
            ),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Makanan Terlaris',
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                ),
                InkResponse(
                  onTap: () {},
                  child: Text(
                    'Selengkapnya',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: greenColor,
                    ),
                  ),
                ),
              ],
            ).padding(horizontal: 16),
            ListView.builder(
              physics: BouncingScrollPhysics(),
              padding: const EdgeInsets.all(16),
              scrollDirection: Axis.horizontal,
              itemCount: 6,
              itemBuilder: (context, index) {
                return FoodCard(
                  image: AssetImage('assets/resto_1.png'),
                ).padding(right: 16);
              },
            ).constrained(height: 240),
            SizedBox(height: mediaQuery.padding.bottom + 16),
          ],
        ),
      ),
    );
  }
}
