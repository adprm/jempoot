import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';
import '../../widgets/cards/jempoot_card.dart';

class HistoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'History',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: ListView(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
        children: [
          _buildItem(
            context,
            onTap: () {
              Navigator.pushNamed(context, '/history-food');
            },
            label: 'Food',
            description: 'Terakhir transaksi: 12/12/2021',
          ),
          _buildItem(
            context,
            onTap: () {
              Navigator.pushNamed(context, '/history-jempoot');
            },
            label: 'Jempoot',
            description: 'Terakhir transaksi: 12/12/2021',
          ),
          _buildItem(
            context,
            onTap: () {
              Navigator.pushNamed(context, '/history-kirim');
            },
            label: 'Kirim',
            description: 'Terakhir transaksi: 12/12/2021',
          ),
        ],
      ),
    );
  }

  Widget _buildItem(
    BuildContext context, {
    required Function() onTap,
    required String label,
    required String description,
  }) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16),
      child: JempootCard(
        onTap: onTap,
        padding: const EdgeInsets.all(16),
        child: Row(
          children: [
            Container(
              height: 60,
              width: 6,
              decoration: BoxDecoration(
                color: orangeColor,
                borderRadius: BorderRadius.circular(defaultBorderRadius),
              ),
            ),
            SizedBox(width: 16),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  label,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  description,
                  style: TextStyle(color: darkGreyColor),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
              ],
            ).expanded(),
          ],
        ),
      ),
    );
  }
}
