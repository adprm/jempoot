import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../core/themes/theme.dart';
import '../notifiers/bottom_nav/bottom_nav_notifier.dart';
import '../widgets/circle_image.dart';
import 'history/history_page.dart';
import 'home/home_page.dart';
import 'profile/profile_page.dart';

class MainPage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final notifier = watch(bottomNavNotifier);

    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        onTap: notifier.changeIndex,
        currentIndex: notifier.pageIndex,
        backgroundColor: Colors.white,
        unselectedItemColor: mediumGreyColor,
        selectedItemColor: orangeColor,
        type: BottomNavigationBarType.fixed,
        selectedLabelStyle: TextStyle(
          fontSize: 11,
          fontWeight: FontWeight.w500,
        ),
        unselectedLabelStyle: TextStyle(
          fontSize: 11,
          fontWeight: FontWeight.w500,
        ),
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.history_edu),
            label: 'Transaksi',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.history),
            label: 'History',
          ),
          BottomNavigationBarItem(
            icon: CircleImage(
              size: 24,
              image: AssetImage('assets/profile_adit.png'),
            ),
            label: 'Profile',
          ),
        ],
      ),
      body: PageView(
        controller: notifier.controller,
        physics: NeverScrollableScrollPhysics(),
        children: [
          HomePage(),
          Container(),
          HistoryPage(),
          ProfilePage(),
        ],
      ),
    );
  }
}
