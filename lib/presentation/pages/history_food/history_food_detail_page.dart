import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';
import '../../widgets/buttons/rounded_button.dart';
import '../../widgets/chips/small_chip.dart';
import '../../widgets/circle_image.dart';
import '../../widgets/rating_stars.dart';

class HistoryFoodDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: orangeColor),
        centerTitle: true,
        title: Text(
          'Food',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RoundedButton(
              horizontalPadding: 48,
              label: 'Beli lagi',
            ),
          ],
        ).padding(all: 16),
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Kode pemesanan: FD-98JM918JQO82',
              style: TextStyle(color: darkGreyColor),
            )
                .center()
                .padding(
                  vertical: 4,
                )
                .decorated(
                  color: lightGreyColor,
                  borderRadius: BorderRadius.circular(defaultBorderRadius),
                ),
            SizedBox(height: 32),
            Row(
              children: [
                Container(
                  height: 80,
                  width: 80,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(defaultBorderRadius),
                    image: DecorationImage(
                      image: AssetImage('assets/resto_4.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(width: 16),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SmallChip(
                      child: Text(
                        'CEPAT SAJI',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 10,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    SizedBox(height: 8),
                    Text(
                      'KFC - Pajajaran Bogor',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(height: 8),
                    Text(
                      'Senin, 12/12/2021',
                      style: TextStyle(fontSize: 12, color: orangeColor),
                    ),
                  ],
                ).expanded(),
              ],
            ),
            SizedBox(height: 32),
            Row(
              children: [
                CircleImage(size: 50).elevation(
                  4,
                  borderRadius: BorderRadius.circular(50),
                ),
                SizedBox(width: 16),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Mas Dav (driver)',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: greenColor,
                      ),
                    ),
                    SizedBox(height: 2),
                    Text(
                      'F 1234 AB',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: darkGreyColor,
                      ),
                    ),
                  ],
                ).expanded(),
              ],
            ),
            SizedBox(height: 32),
            Row(
              children: [
                RatingStars(voteAverage: 4, starSize: 14)
                    .padding(horizontal: 10, vertical: 4)
                    .decorated(
                      color: lightGreyColor,
                      borderRadius: BorderRadius.circular(50),
                    ),
              ],
            ),
            SizedBox(height: 16),
            Text(
              'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s',
              style: TextStyle(
                color: darkGreyColor,
                fontSize: 12,
                height: 1.5,
              ),
            )
                .padding(
                  horizontal: 16,
                  vertical: 12,
                )
                .decorated(
                  border: Border.all(color: mediumGreyColor),
                  borderRadius: BorderRadius.circular(defaultBorderRadius),
                ),
            SizedBox(height: 32),
            Row(
              children: [
                Text(
                  'Rincian',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
            SizedBox(height: 12),
            _buildRow(title: 'Produk', price: 25000),
            _buildRow(title: 'Ongkir', price: 25000),
            _buildRow(title: 'Biaya Admin', price: 25000),
            _buildRow(title: 'Diskon', price: -25000, priceColor: greenColor),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total Pembayaran',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Text(
                  'Rp 123.000',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: greenColor),
                  textAlign: TextAlign.right,
                ),
              ],
            ),
            SizedBox(height: 64),
          ],
        ),
      ),
    );
  }

  Widget _buildRow({
    required String title,
    required int price,
    Color priceColor = Colors.black,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(title, style: TextStyle(color: darkGreyColor)),
        Text(
          'Rp $price',
          style: TextStyle(fontWeight: FontWeight.w500, color: priceColor),
          textAlign: TextAlign.right,
        ),
      ],
    ).padding(bottom: 8);
  }
}
