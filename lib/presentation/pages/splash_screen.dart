import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            height: mediaQuery.size.height,
            width: mediaQuery.size.width,
            child: Image.asset(
              'assets/bg_splashscreen.png',
              fit: BoxFit.cover,
            ),
          ),
          Center(
            child: SvgPicture.asset('assets/logo.svg'),
          ),
        ],
      ),
    );
  }
}
