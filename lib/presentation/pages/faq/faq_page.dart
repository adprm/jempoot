import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';

class FaqPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: orangeColor),
        centerTitle: true,
        title: Text(
          'FAQ',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: ListView.builder(
        physics: BouncingScrollPhysics(),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
        itemCount: 6,
        itemBuilder: (context, index) {
          return ExpandablePanel(
            collapsed: ExpandableButton(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Apa itu apa?',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                  ).expanded(),
                  Icon(
                    Icons.keyboard_arrow_down,
                    size: 32,
                    color: mediumGreyColor,
                  ),
                ],
              ).padding(all: 16),
            ),
            expanded: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ExpandableButton(
                  theme: ExpandableThemeData(
                    iconPadding: const EdgeInsets.all(16),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Apa itu apa?',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ).expanded(),
                      Icon(
                        Icons.keyboard_arrow_up,
                        size: 32,
                        color: mediumGreyColor,
                      ),
                    ],
                  ).padding(all: 16),
                ),
                Text(
                  'Lorem ipsum dolor sit amet. Lorem ipsum is simply dummy text of the printing.',
                  style: TextStyle(color: darkGreyColor),
                ).padding(horizontal: 16, bottom: 16),
              ],
            ),
          )
              .ripple()
              .decorated(color: lightGreyColor)
              .clipRRect(all: defaultBorderRadius)
              .padding(bottom: 16);
        },
      ).backgroundImage(
        DecorationImage(
          image: AssetImage('assets/bg_kyuubi.png'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
