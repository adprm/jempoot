import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';
import '../../widgets/bottom_sheet/select_location.dart';
import '../../widgets/buttons/circle_button_alt.dart';
import '../../widgets/food_card.dart';

class FoodPage extends StatelessWidget {
  const FoodPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: orangeColor),
        centerTitle: false,
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Jl. Cibereum Petir',
              style: TextStyle(
                color: darkGreyColor,
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Icon(Icons.bookmark_outline_rounded, color: orangeColor),
            ),
          ],
        )
            .padding(horizontal: 16, vertical: 4)
            .decorated(
              color: lightGreyColor,
              borderRadius: BorderRadius.circular(defaultBorderRadius),
            )
            .expanded()
            .gestures(
          onTap: () {
            showMaterialModalBottomSheet(
              context: context,
              builder: (context) => SelectLocation(),
              backgroundColor: Colors.transparent,
            );
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
              height: 125,
              width: mediaQuery.size.width,
              decoration: BoxDecoration(
                color: mediumGreyColor,
                borderRadius: BorderRadius.circular(defaultBorderRadius),
                image: DecorationImage(
                  image: AssetImage('assets/banner_1.png'),
                  fit: BoxFit.cover,
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0, 20),
                    spreadRadius: -15,
                    blurRadius: 12,
                  ),
                ],
              ),
            ),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Cari Makanan', style: TextStyle(color: darkGreyColor)),
                Icon(Icons.search, color: darkGreyColor),
              ],
            ).padding(horizontal: 16, vertical: 12).decorated(
              color: lightGreyColor,
              borderRadius: BorderRadius.circular(defaultBorderRadius),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  offset: Offset(0, 18),
                  spreadRadius: -15,
                  blurRadius: 12,
                ),
              ],
            ).padding(horizontal: 16),
            SizedBox(height: 32),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleButtonAlt(
                  onPressed: () {
                    Navigator.pushNamed(context, '/food-list');
                  },
                  icon: SvgPicture.asset('assets/ic_terdekat.svg'),
                  label: 'Terdekat',
                ).padding(horizontal: 8),
                CircleButtonAlt(
                  onPressed: () {
                    Navigator.pushNamed(context, '/food-list');
                  },
                  icon: SvgPicture.asset('assets/ic_hot_deals.svg'),
                  label: 'Lagi Promo',
                ).padding(horizontal: 8),
                CircleButtonAlt(
                  onPressed: () {
                    Navigator.pushNamed(context, '/food-list');
                  },
                  icon: SvgPicture.asset('assets/ic_ongkir.svg'),
                  label: 'Gratis Ongkir',
                ).padding(horizontal: 8),
                CircleButtonAlt(
                  onPressed: () {
                    Navigator.pushNamed(context, '/food-list');
                  },
                  icon: SvgPicture.asset('assets/ic_food.svg'),
                  label: 'Semua',
                ).padding(horizontal: 8),
              ],
            ).padding(horizontal: 16),
            SizedBox(height: 32),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Makanan Terlaris',
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                ),
                InkResponse(
                  onTap: () {},
                  child: Text(
                    'Selengkapnya',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: greenColor,
                    ),
                  ),
                ),
              ],
            ).padding(horizontal: 16),
            ListView.builder(
              physics: BouncingScrollPhysics(),
              padding: const EdgeInsets.all(16),
              scrollDirection: Axis.horizontal,
              itemCount: 6,
              itemBuilder: (context, index) {
                return FoodCard(
                  image: AssetImage('assets/resto_1.png'),
                ).padding(right: 16);
              },
            ).constrained(height: 240),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Makanan Paling Murah',
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                ),
                InkResponse(
                  onTap: () {},
                  child: Text(
                    'Selengkapnya',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: greenColor,
                    ),
                  ),
                ),
              ],
            ).padding(horizontal: 16),
            ListView.builder(
              physics: BouncingScrollPhysics(),
              padding: const EdgeInsets.all(16),
              scrollDirection: Axis.horizontal,
              itemCount: 6,
              itemBuilder: (context, index) {
                return FoodCard(
                  image: AssetImage('assets/resto_5.png'),
                ).padding(right: 16);
              },
            ).constrained(height: 240),
            SizedBox(height: mediaQuery.padding.bottom + 16),
          ],
        ),
      ),
    );
  }
}
