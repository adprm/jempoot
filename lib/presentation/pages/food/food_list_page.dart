import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';
import '../../widgets/bottom_sheet/select_location.dart';
import '../../widgets/chips/small_chip.dart';

class FoodListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: orangeColor),
        centerTitle: false,
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Jl. Cibereum Petir',
              style: TextStyle(
                color: darkGreyColor,
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Icon(Icons.bookmark_outline_rounded, color: orangeColor),
            ),
          ],
        )
            .padding(horizontal: 16, vertical: 4)
            .decorated(
              color: lightGreyColor,
              borderRadius: BorderRadius.circular(defaultBorderRadius),
            )
            .expanded()
            .gestures(
          onTap: () {
            showMaterialModalBottomSheet(
              context: context,
              builder: (context) => SelectLocation(),
              backgroundColor: Colors.transparent,
            );
          },
        ),
      ),
      body: ListView.builder(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
        itemCount: 3,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/food-resto');
            },
            borderRadius: BorderRadius.circular(defaultBorderRadius),
            child: Row(
              children: [
                Container(
                  height: 120,
                  width: 120,
                  decoration: BoxDecoration(
                    color: lightGreyColor,
                    borderRadius: BorderRadius.circular(defaultBorderRadius),
                    image: DecorationImage(
                      image: AssetImage('assets/resto_1.png'),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black26,
                        offset: Offset(0, 20),
                        spreadRadius: -15,
                        blurRadius: 12,
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 16),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SmallChip(
                      backgroundColor: orangeColor,
                      child: Text(
                        'CEPAT SAJI',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 10,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    SizedBox(height: 8),
                    Text(
                      'Burger Mas Dav - Jl. Cilok',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ).width(mediaQuery.size.width - 120 - 48),
                    SizedBox(height: 8),
                    ListView(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      physics: BouncingScrollPhysics(),
                      children: [
                        SmallChip(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(Icons.star, size: 14, color: Colors.amber),
                              SizedBox(width: 2),
                              Text(
                                '4.0',
                                style: TextStyle(fontSize: 11),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: 4),
                        SmallChip(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.location_on,
                                size: 14,
                                color: darkGreyColor,
                              ),
                              SizedBox(width: 2),
                              Text(
                                '1 km',
                                style: TextStyle(fontSize: 11),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: 4),
                        SmallChip(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.watch_later,
                                size: 14,
                                color: darkGreyColor,
                              ),
                              SizedBox(width: 2),
                              Text(
                                '30 min',
                                style: TextStyle(fontSize: 11),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ).constrained(
                      height: 20,
                      width: mediaQuery.size.width - 120 - 48,
                    ),
                    SizedBox(height: 24),
                    Row(
                      children: [
                        SmallChip(
                          child: Text(
                            'Gratis Ongkir',
                            style: TextStyle(fontSize: 12, color: Colors.white),
                          ),
                          backgroundColor: lightGreenColor,
                          borderRadius: defaultBorderRadius,
                        ),
                        SizedBox(width: 8),
                        SmallChip(
                          child: Text(
                            'Promo',
                            style: TextStyle(fontSize: 12, color: Colors.white),
                          ),
                          backgroundColor: lightGreenColor,
                          borderRadius: defaultBorderRadius,
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ).padding(bottom: 24);
        },
      ),
    );
  }
}
