import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';
import '../../widgets/buttons/circle_button.dart';
import '../../widgets/buttons/rounded_button.dart';
import '../../widgets/circle_image.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Profile',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CircleImage(
              size: 140,
              image: AssetImage('assets/profile_adit.png'),
            ),
            SizedBox(height: 16),
            Text(
              'Adit Skut',
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
            ),
            SizedBox(height: 8),
            Text(
              '0812 3456 7890',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 18,
                color: orangeColor,
              ),
            ),
            SizedBox(height: 32),
            RoundedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/edit-profile');
              },
              label: 'Edit Profile',
              horizontalPadding: 48,
            ),
            SizedBox(height: 32),
            Divider(color: mediumGreyColor).padding(horizontal: 64),
            InkResponse(
              onTap: () {
                Navigator.pushNamed(context, '/contact');
              },
              child: Text('Contact Us').padding(vertical: 16, horizontal: 32),
            ),
            Divider(color: mediumGreyColor).padding(horizontal: 64),
            InkResponse(
              onTap: () {
                Navigator.pushNamed(context, '/faq');
              },
              child: Text('FAQ').padding(vertical: 16, horizontal: 32),
            ),
            SizedBox(height: 32),
            Column(
              children: [
                CircleButton(
                  child: Icon(Icons.exit_to_app, color: Colors.white),
                ),
                SizedBox(height: 12),
                Text('Logout', style: TextStyle(color: orangeColor)),
              ],
            ),
          ],
        ).center(),
      ),
    );
  }
}
