import 'dart:ui';

const Color orangeColor = const Color(0xffEC4515);
const Color lightGreenColor = const Color(0xff99CF16);
const Color greenColor = const Color(0xff047C04);
const Color yellowColor = const Color(0xffFFCC01);
const Color lightGreyColor = const Color(0xffF0F0F0);
const Color mediumGreyColor = const Color(0xffA4A4A4);
const Color darkGreyColor = const Color(0xff707070);

const double defaultBorderRadius = 6;
const double mediumBorderRadius = 10;
